<resources xmlns:tools="http://schemas.android.com/tools" tools:ignore="MissingTranslation">
    <string name="app_name" translatable="false">Syncplay</string>

    <!-- Main Activity's most essential strings -->
    <string name="tab_connect">Connect</string>
    <string name="tab_settings">Settings</string>
    <string name="tab_about">About</string>

    <string name="connect_username_a">Type your username:</string>
    <string name="connect_username_b">Username</string>
    <string name="connect_username_c">A name of your choice</string>
    <string name="connect_roomname_a">Insert room name:</string>
    <string name="connect_roomname_b">Room name</string>
    <string name="connect_roomname_c">Room where you and your friends watch </string>
    <string name="connect_server_a">Select Syncplay server:</string>
    <string name="connect_server_b">Server address</string>
    <string name="connect_server_c">Make sure that you and your friends\njoin the same server.</string>
    <string name="connect_button">JOIN / CREATE ROOM</string>
    <string name="connect_footnote">Syncplay\'s Unofficial Android Client</string>
    <string name="connect_username_empty_error">Username shouldn\'t be empty</string>
    <string name="connect_roomname_empty_error">Room name shouldn\'t be empty</string>
    <string name="connect_enter_custom_server">Enter Custom Server</string>
    <string name="connect_custom_server_password">Password (Empty if not required)</string>
    <string name="connect_port">Port</string>

    <!-- Room Activity Strings -->
    <string name="room_startup_hint">"Insert a new video from storage by clicking on the button with the following icon "</string>
    <string name="room_type_message">Type your message…</string>
    <string name="room_send_button">SEND</string>
    <string name="room_ready">Ready</string>
    <string name="room_details">Show Details</string>
    <string name="room_ping_connected">Connected - PING: %s ms </string>
    <string name="room_ping_disconnected">DISCONNECTED</string>
    <string name="room_overflow_sub">Load Subtitle File…</string>
    <string name="room_overflow_cutout">Cut-out (Notch) Mode</string>
    <string name="room_overflow_ff">Fast Seek Buttons</string>
    <string name="room_overflow_msghistory">Message History</string>
    <string name="room_overflow_settings">Settings</string>
    <string name="room_empty_message_error">Type something!</string>
    <string name="room_attempting_connect">Attempting to connect to %1s:%2s</string>
    <string name="room_connected_to_server">Connected to the server.</string>
    <string name="room_connection_failed">Connecting failed.</string>
    <string name="room_attempting_reconnection">Lost connection to the server. Attempting to reconnect…</string>
    <string name="room_guy_played">%s unpaused</string>
    <string name="room_guy_paused">%s paused</string>
    <string name="room_seeked">%1s jumped from %2s to %3s</string>
    <string name="room_rewinded">Rewinded due to time difference with %s</string>
    <string name="room_guy_left">%s left the room.</string>
    <string name="room_guy_joined">%s joined the room.</string>
    <string name="room_isplayingfile">%1s is playing \'%2s\' (%3s)</string>
    <string name="room_you_joined_room">You have joined the room: %s</string>
    <string name="room_scaling_fit_screen">Resize Mode: FIT TO SCREEN</string>
    <string name="room_scaling_fixed_width">Resize Mode: FIXED WIDTH</string>
    <string name="room_scaling_fixed_height">Resize Mode: FIXED HEIGHT</string>
    <string name="room_scaling_fill_screen">Resize Mode: FILL SCREEN</string>
    <string name="room_scaling_zoom">Resize Mode: Zoom</string>
    <string name="room_sub_track_changed">Subtitle Track changed to: %s</string>
    <string name="room_audio_track_changed">Audio Track changed to: %s</string>
    <string name="room_audio_track_not_found">No audio found !</string>
    <string name="room_sub_track_disable">Disable Subtitles</string>
    <string name="room_track_track">Track</string>
    <string name="room_sub_track_notfound">No subtitles found !</string>
    <string name="room_details_current_room">Current Room: %s</string>
    <string name="room_details_nofileplayed">(No file being played)</string>
    <string name="room_details_file_properties">Duration: %1s - Size: %2s MBs</string>
    <string name="disconnected_notice_header">DISCONNECTED</string>
    <string name="disconnected_notice_a">You have been disconnected from the server.\nThis may be due to a problem in your internet connection or an issue with the server you\'re connecting to. If the problem persists, change the server.</string>
    <string name="disconnected_notice_b">Attempting to reconnect…</string>

    <!-- Settings Strings -->
    <string name="settings_categ_general">General Settings</string>
    <string name="settings_categ_player">Player Settings</string>
    <string name="settings_categ_room">Room Settings</string>
    <string name="settings_categ_video">Video Settings</string>
    <string name="settings_categ_misc">Miscellaneous</string>
    <string name="setting_remember_join_info_title">Remember joining info</string>
    <string name="setting_remember_join_info_summary">Enabled by default. This will allow SyncPlay to save your last connecting info for the next time you open the app.</string>
    <string name="setting_display_language_title">Display Language</string>
    <string name="setting_display_language_summry">Select your language of choice with which Syncplay is displayed.</string>
    <string name="setting_display_language_toast">Changed language to: %s. Restart app for the setting to take full effect.</string>
    <string name="setting_use_buffer_title">Use Custom Buffer Sizes</string>
    <string name="setting_use_buffer_summary">If you are not satisfied with the player\'s video load times before and during playback, you can use custom buffer sizes (Use at your own risk).</string>
    <string name="setting_max_buffer_title">Custom max buffer size</string>
    <string name="setting_max_buffer_summary">Default is 50 (50000 milliseconds). This determines the maximum buffer size before starting to play the video. If you don\'t know what this is, don\'t change it.</string>
    <string name="setting_min_buffer_summary">Default is 15 (15000 milliseconds). Decrease this value to play video faster but there is a possibility that the player may fail or even crash. Change at your own risk.</string>
    <string name="setting_min_buffer_title">Custom min buffer size</string>
    <string name="setting_playback_buffer_summary">Default is 2500 milliseconds. This represents the buffer size when SEEKING or UNPAUSING the video. Change this if you\'re not satisfied with the small delay when seeking video.</string>
    <string name="setting_playback_buffer_title">Custom playback buffer size (ms)</string>
    <string name="setting_ready_firsthand_summary">Enable this if you want to be set automatically as ready once you enter the room.</string>
    <string name="setting_ready_firsthand_title">Set as ready first-hand</string>
    <string name="setting_rewind_threshold_summary">If someone is behind with the value you select here, your video will be rewinded to match with the one behind.</string>
    <string name="setting_rewind_threshold_title">Rewind thresold</string>
    <string name="setting_subtitle_size_summary">Default is 18. This will change the subtitles size.</string>
    <string name="setting_subtitle_size_title">Subtitle Size</string>
    <string name="setting_tls_summary">If a server supports TLS secure connection, Syncplay Android will attempt to connect to it through TLS. (Not available yet)</string>
    <string name="setting_tls_title">Use Secure Connection (TLSv1.3) [Available Soon]</string>
    <string name="setting_resetdefault_title">Reset Default Settings</string>
    <string name="setting_resetdefault_summary">Reset everything to its default value (Recommended)</string>

    <!-- UI Settings Strings -->
    <string name="uisetting_apply">APPLY</string>
    <string name="uisetting_timestamp_summary">Disable this to hide the timestamps at the beginning of chat messages.</string>
    <string name="uisetting_timestamp_title">Chat timestamps</string>
    <string name="uisetting_overview_alpha_summary">Default is 30 (Almost transparent), change this if you want to make the room details\' panel more readable by increasing opacity.</string>
    <string name="uisetting_overview_alpha_title">Room Details Background Opacity</string>
    <string name="uisetting_messagery_alpha_summary">Default is 0 (Transparent). Maximum is 255 (100% Opaque). Increase readability of the messages by making background more opaque.</string>
    <string name="uisetting_messagery_alpha_title">Messages\' background opacity</string>
    <string name="uisetting_msgsize_summary">Changes message text size. Default is 11.</string>
    <string name="uisetting_msgsize_title">Message font size</string>
    <string name="uisetting_msgcount_summary">Default is 12. Limits the message number count to this value.</string>
    <string name="uisetting_msgcount_title">Message max count</string>
    <string name="uisetting_msglife_summary">When receiving a chat message or room message, it will begin fading out for the amount of time set below.</string>
    <string name="uisetting_msglife_title">Display duration for Chat messages</string>
    <string name="uisetting_timestamp_color_summary">Customize the text color of message timestamps (Default is Pink)</string>
    <string name="uisetting_timestamp_color_title">Timestamp Text Color</string>
    <string name="uisetting_self_color_summary">Customize the text color of your name tag (Default is Red)</string>
    <string name="uisetting_self_color_title">Self Tag Color</string>
    <string name="uisetting_friend_color_summary">Customize the text color of friends\' name tags (Default is Blue)</string>
    <string name="uisetting_friend_color_title">Friend Tag Text Color</string>
    <string name="uisetting_system_color_summary">Customize the text color of system room messages (Default is White)</string>
    <string name="uisetting_system_color_title">System Message Text Color</string>
    <string name="uisetting_human_color_summary">Customize the text color of user messages (Default is White)</string>
    <string name="uisetting_human_color_title">User Message Text Color</string>


    <string name="uisetting_resetdefault_summary">Reset all of the above settings to default.</string>
    <string name="uisetting_resetdefault_title">Reset Default Settings</string>


    <!--List of Languages identifier strings used for, meh, just a toast... -->
    <string name="en">English</string>
    <string name="ar">Arabic</string>

    <!--List of Languages and their Tags-->
    <string-array name="Lang">
        <item>English</item>
        <item>العربية</item>
    </string-array>

    <string-array name="LangValues">
        <item>"en"</item>
        <item>"ar"</item>
    </string-array>
</resources>